## ## silverstripe-australia/developer-exercise ##

**Technology stack:**  

* Laravel5.1
* Bootstrap
* Nginx 1.8
* Ubuntu 14
* Php-FPM 1. 

**Live demo**

*http://www.demo1.tofa.me*

##Installation instraction

1) Copy the source code to the document root

2) Set document root to /<laravelsource location>/public

3) Change /<laravelsource location>/.env and change

* DB_HOST=<mysql host name>
* DB_DATABASE=<database name>
* DB_USERNAME=<mysql username>
* DB_PASSWORD=<mysql password>

4) Go to /<laravelsource location>/ and run

php artisan migrate

5) Load the home page.

6) To update the site, use

* username: a@b.com
* Password: abc@123