<?php

namespace App\Http\Controllers ;

use App\Http\Controllers\Controller;


use Auth;
use App\Page;

use Request;
use App\Http\Requests\PageRequest;

use DB;

class PagesController extends Controller
{

   function __construct() {
       $this->middleware('auth', ['only' => ['create', 'edit']]);
   }
    

	public function index ()
	{

        $content=Page::where('ishomepage',1)->first();
        
     
		return view('pages.index',compact('content'));

	}

	public function show($id)
    {
    
        $content=Page::where('id',$id)->first();
             
        return view('pages.index',compact('content'));
    
    }
    
   
    public function create()
    {
    

       return view('pages.create');    	
    
    }

//
    public function store(PageRequest $request)
    {
    

    	$input=$this->excludeCsrfToken();

        $this->updateHomePage($input['ishomepage']);
       
        Page::create($input);
        \Session::flash('notice',"Your page has been created!");

    	return redirect('pages');
    
    }

  
    public function edit($id)
    {
    
        $page=Page::findOrFail($id);

        return view('pages.edit',compact('page'));
    
    }

    public function update($id,PageRequest $request )
    {
    
        $input=$this->excludeCsrfToken();

        $this->updateHomePage($input['ishomepage']);
  
        $pageno=Page::findOrFail($id);
        $pageno->update($input);
        session()->flash('notice',"Your page has been updated!");

        return redirect('pages');
    
    }

    private function updateHomePage($ishomepage)
    {
    
       if ($ishomepage)
        {
           page::RemovePreviousHomepage();
        } 
    
    }

    private function excludeCsrfToken()
    {
        return Request::except('_token');
    }
    
    
    
    
    
    


}