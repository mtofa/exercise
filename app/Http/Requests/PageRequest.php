<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Request::segment(2);
        return [
            'name' => 'required|max:255|unique:pages,name,'.$id, //bypass current id unique check during update
            'ishomepage' => 'required',
            'content'=>'required|min:3'
        ];
    }
}
