<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class Page extends Model
{

	protected $table = 'pages';

	//Prevent mass assignment
	protected $fillable = ['name','ishomepage','content'];


    public function scopeRemovePreviousHomepage($query)
    {
    
    	$query->where('ishomepage', 1)->update(array('ishomepage' => 0));
    
    }

    public function scopeGetHomePage($query)
    {
    
    	$query->where('ishomepage',1)->first();
    
    }
    
    
 
    


}