<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
     
      $this->leftNavigationBar();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /*
    * Compose the left navigation bar
    */
    private function leftNavigationBar()
    {
    
      view()->composer('partials.leftnav',function($view){

      $view->with('pagetitles',DB::table('pages')->select('name','id')->get());

      });  
    
    }
    


}
