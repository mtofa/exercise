<!-- resources/views/auth/login.blade.php -->

@extends('layout')

@section('content')

<h1>Login</h1>

<div class="alert">
@if($errors->any())

   <ul class="list-group" >
   
     @foreach($errors->all() as $error)
        <li class="list-group-item list-group-item-danger">{{$error}}</li>
     @endforeach
   </ul>
 @endif

 </div>

<div class="form-group">
<form method="POST" action="{{url('/')}}/auth/login">
    {!! csrf_field() !!}

    <div>
        Email
        <input class="form-control"  type="email" name="email" value="a@b.com">
    </div>

    <div>
        Password
        <input class="form-control"  type="password" name="password" id="password" value="abc@123">
    </div>

    <br/><br/>
    <div>
        <button type="submit" class="btn btn-default" >Login</button>
    </div>
</form>
</div>



@stop