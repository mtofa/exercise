<!-- resources/views/auth/register.blade.php -->

@extends('layout')

@section('content')

<h1> Register </h1>



<form method="POST" action="{{url('/')}}/auth/register">
    {!! csrf_field() !!}


    <div class="form-group">
    {!! Form::label('name','Name:') !!}

    {!! Form::text('name',null,['class'=>'form-controller']) !!}
    </div>
    <div class="form-group">
    {!! Form::label('fullname','Full Name:') !!}

    {!! Form::text('fullname',null,['class'=>'form-controller']) !!}
    </div>

   <div class="form-group">
    {!! Form::label('email','Email:') !!}

    {!! Form::email('email',null,['class'=>'form-controller']) !!}
    </div>

    <div class="form-group">
    {!! Form::label('password','Password:') !!}

    {!! Form::password('password',null,['class'=>'form-controller']) !!}
    </div>

    <div class="form-group">
    {!! Form::label('password_confirmation','Confirm Password:') !!}

    {!! Form::password('password_confirmation',null,['class'=>'form-controller']) !!}
    </div>

     <div class="form-group">
    {!! Form::label('address','Address:') !!}

    {!! Form::textarea('address',null,['class'=>'form-controller']) !!}
    </div>

    <div class="form-group">
    {!! Form::submit("Register",['class'=>'btn btn-primary form-controller']) !!}
    </div>
</form>



@stop