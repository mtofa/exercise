<!DOCTYPE html>
<html>
<link rel="stylesheet" href="{{ url() }}/css/bootstrap.min.css" type="text/css"/>

<body>

<div class="row">
	<nav class="navbar navbar-default" role="navigation">
		<div class="col-md-8">
		
		<div class="navbar-header">
					 
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		
		    	 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
		
			</button> <a class="navbar-brand" href="{{url('/')}}">Home</a>
			
		
		</div>

				
			
		</div>

		<div class="col-md-4">
		@if (Auth::check())
		<b>Usename:</b> {{Auth::user()->email}}
		@endif 
		</div>

		</nav>

	</div>

<div class="container-fluid">
<div class="row">
<div class="col-md-4">
<!-- Left menu -->

@include('partials.leftnav')
<!-- left menu ends  -->
		
                @if (Auth::check()) 
                     
                     <a href="{{url('/auth/logout')}}" class="btn btn-primary" role="button"> Logout </a>
                     <a href="{{url('/pages/create')}}"  class="btn btn-info" role="button" > Addnewpage </a>
                @else
                     <a href="{{url('/auth/login')}}" class="btn btn-primary" role="button"> Login </a>
                @endif
				
		
		</div>

<div class="col-md-8">

@if (Session::has('notice'))
  
 
   <div class="alert alert-success">

   {{session('notice')}}

    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>

   </div>
   
   <script src="{{ url() }}/js/jquery.min.js" ></script>
   <script src="{{ url() }}/js/bootstrap.min.js" ></script>
   <script type="text/javascript"> 
       $('div.alert').delay(3000).slideUp(300);
   </script>

@endif

@yield('content')


@yield('footer')

</div>
	
</div>

</body>

</html>