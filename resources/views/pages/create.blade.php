@extends('layout')

@section('content')


@if($errors->any())

   <ul class="list-group" >
   
     @foreach($errors->all() as $error)
        <li class="list-group-item list-group-item-danger">{{$error}}</li>
     @endforeach
   </ul>
 

@endif

<!-- <form > -->

<fieldset>

<!-- Form Name -->
<legend>Create Page</legend>
{!!Form::open(['url'=>'pages'])!!}

@include('pages._form',['button_value'=>'Add Page'])

{!!Form::close()!!}





@stop