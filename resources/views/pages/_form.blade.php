
<div class="form-horizontal">



<div class="form-group">

  {!! Form::label('name','Name',['class'=>'col-md-4 control-label']) !!}


  <div class="col-md-4">
  {!! Form::text('name',null,['class'=>'form-control input-md','placeholder'=>'Page name']) !!}
  <!-- <input id="textinput" name="name" type="text" placeholder="Page Name" class="form-control input-md">
   -->  
  </div>
</div>
<!-- Multiple Radios -->
<div class="form-group">
  {!! Form::label('ishomepage','Home Page?',['class'=>'col-md-4 control-label']) !!}
  <div class="col-md-4">
  <div class="radio">
    <label for="radios-0">
      {!!Form::radio('ishomepage',1)!!}
      Yes
    </label>
	</div>
  <div class="radio">
    <label for="radios-1">
       {!!Form::radio('ishomepage',0,true)!!}
      No
    </label>
	</div>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
     {!! Form::label('content','Content',['class'=>'col-md-4 control-label']) !!}
 
  <div class="col-md-4">  

     {!! Form::textarea('content',null,['class'=>'form-control input-md','placeholder'=>'Page Content']) !!}


  </div>
</div>


<div class="form-group">
<div class="col-md-4">
{!! Form::submit($button_value,['class'=>'btn btn-primary form-controller']) !!}
</div>
</div>


</div>
</fieldset>
