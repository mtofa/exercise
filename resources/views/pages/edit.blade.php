@extends('layout')

@section('content')

@if($errors->any())

   <ul class="list-group" >
   
     @foreach($errors->all() as $error)
        <li class="list-group-item list-group-item-danger">{{$error}}</li>
     @endforeach
   </ul>
 

@endif

<!-- <form > -->

<fieldset>

<!-- Form Name -->
<legend>Edit Page</legend>


{!! Form::model($page,['method'=>'Patch','action'=>['PagesController@update',$page->id]]) !!}

@include('pages._form',['button_value'=>'Save'])

{!!Form::close()!!}





@stop